package at.technikum.bicss.ese.androatuin.bluetoothtesting;

import java.io.InputStream;

import javax.microedition.io.StreamConnection;

public class ProcessConnectionThread
    implements Runnable
{

    private StreamConnection mConnection;

    public ProcessConnectionThread (StreamConnection connection)
    {
        mConnection = connection;
    }

    @Override
    public void run ()
    {
        try
        {
            // prepare to receive data
            InputStream inputStream = mConnection.openInputStream ();

            System.out.println ("waiting for input");

            int counter = 0;

            StringBuilder out;

            out = new StringBuilder ();

            while (true)
            {
                int read;
                read = inputStream.read ();

                if (read > 0)
                {
                    out.append ((char)read);
                }

                System.out.println (read);

                counter++;

                if (counter == 5)
                {
                    counter = 0;
                    
                    System.out.println ("-------END-------");
                    System.out.println ("------START------");

                    out = new StringBuilder ();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace ();
        }
    }

}
