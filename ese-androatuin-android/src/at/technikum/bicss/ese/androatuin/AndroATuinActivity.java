package at.technikum.bicss.ese.androatuin;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AndroATuinActivity
    extends Activity
{
    // Debugging
    private static final String               TAG                    = "[AndroATuin][Main]";
    private static final boolean              D                      = false;

    // Intent request codes
    private static final int                  REQUEST_CONNECT_DEVICE = 1;
    private static final int                  REQUEST_ENABLE_BT      = 2;

    // Message types sent from the BluetoothChatService Handler
    public static final int                   MESSAGE_STATE_CHANGE   = 1;
    public static final int                   MESSAGE_READ           = 2;
    public static final int                   MESSAGE_WRITE          = 3;
    public static final int                   MESSAGE_DEVICE_NAME    = 4;
    public static final int                   MESSAGE_TOAST          = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String                DEVICE_NAME            = "device_name";
    public static final String                TOAST                  = "toast";

    // Buttons
    private Button                            touchButton            = null;
    private Button                            sensoryButton          = null;

    // Local Bluetooth adapter
    private BluetoothAdapter                  bluetoothAdapter       = null;

    // Service responsible for the bluetooth connection
    private static AndroATuinBluetoothService bluetoothService       = null;

    private Handler                           handler                = new Handler ()
                                                                     {
                                                                         @Override
                                                                         public void handleMessage (final Message message)
                                                                         {
                                                                             switch (message.what) {
                                                                                 case MESSAGE_STATE_CHANGE:
                                                                                     if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + message.arg1);
                                                                                     switch (message.arg1) {
                                                                                     case AndroATuinBluetoothService.STATE_CONNECTED:
                                                                                         Toast.makeText(getApplicationContext(), "Connected!", Toast.LENGTH_SHORT).show();
                                                                                         break;
                                                                                     case AndroATuinBluetoothService.STATE_CONNECTING:
                                                                                         Toast.makeText(getApplicationContext(), "Connecting...", Toast.LENGTH_SHORT).show();
                                                                                         break;
                                                                                     case AndroATuinBluetoothService.STATE_LISTEN:
                                                                                     case AndroATuinBluetoothService.STATE_NONE:
                                                                                         break;
                                                                                     }
                                                                                     break;
                                                                                 case MESSAGE_WRITE:
                                                                                     
                                                                                     if (D)
                                                                                     {
                                                                                         Log.d (TAG, "I sent = " + message.obj);
                                                                                     }

                                                                                     break;
                                                                                 case MESSAGE_READ:
                                                                                     break;
                                                                                 case MESSAGE_DEVICE_NAME:
                                                                                     break;
                                                                                 case MESSAGE_TOAST:
                                                                                     Toast.makeText(getApplicationContext(), message.getData().getString(TOAST),
                                                                                                    Toast.LENGTH_SHORT).show();
                                                                                     break;
                                                                                 }
                                                                         }
                                                                     };

    /** Called when the activity is first created. */
    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.main);

        touchButton = (Button)findViewById (R.id.button1);

        touchButton.setOnClickListener (new View.OnClickListener ()
        {
            public void onClick (View v)
            {
                Intent myIntent = new Intent (v.getContext (), AndroATuinTouchActivity.class);
                startActivityForResult (myIntent, 0);
            }
        });

        sensoryButton = (Button)findViewById (R.id.button2);

        sensoryButton.setOnClickListener (new View.OnClickListener ()
        {
            public void onClick (View v)
            {
                Intent myIntent = new Intent (v.getContext (), AndroATuinSensoryActivity.class);
                startActivityForResult (myIntent, 0);
            }
        });

        // Get local Bluetooth adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter ();

        // If the adapter is null, then Bluetooth itself is not supported
        if (bluetoothAdapter == null)
        {
            Toast.makeText (this, "Bluetooth is not available", Toast.LENGTH_LONG).show ();
            finish ();
            return;
        }

    }

    @Override
    public void onStart ()
    {
        super.onStart ();

        if (D)
        {
            Log.e (TAG, "Entering: onStart ()");
        }

        // Check if Bluetooth is enabled
        if (!bluetoothAdapter.isEnabled ())
        {
            Intent enableIntent = new Intent (BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult (enableIntent, REQUEST_ENABLE_BT);
        }
        else
        {
            // Initialize the BluetoothChatService to perform bluetooth connections
            if (bluetoothService == null)
            {
                bluetoothService = new AndroATuinBluetoothService (this, handler);
            }
        }

        if (D)
        {
            Log.e (TAG, "Leaving:  onStart ()");
        }
    }

    @Override
    public synchronized void onPause ()
    {
        super.onPause ();
        if (D)
        {
            Log.e (TAG, "Entering: onPause ()");
            Log.e (TAG, "Leaving:  onPause ()");
        }
    }

    @Override
    public void onStop ()
    {
        super.onStop ();
        if (D)
        {
            Log.e (TAG, "Entering: onStop ()");
            Log.e (TAG, "Leaving:  onStop ()");
        }
    }

    @Override
    public void onDestroy ()
    {
        super.onDestroy ();
        if (D)
        {
            Log.e (TAG, "Entering: onDestroy ()");
            Log.e (TAG, "Leaving:  onDestroy ()");
        }
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        if (D)
        {
            Log.e (TAG, "Entering: onActivityResult ()");
            Log.d (TAG, "Request Code = " + requestCode);
        }

        switch (requestCode)
        {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK)
                {
                    // Get the device MAC address
                    String address = data.getExtras ().getString (DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BluetoothDevice object
                    BluetoothDevice device = bluetoothAdapter.getRemoteDevice (address);
                    // Attempt to connect to the device
                    bluetoothService.connect (device);
                    
                    enableButtons ();
                }
            break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK)
                {
                    // Initialize the BluetoothChatService to perform bluetooth connections
                    bluetoothService = new AndroATuinBluetoothService (this, handler);
                    
                    // Bluetooth is now enabled, so set up a chat session
                    enableButtons ();
                }
                else
                {
                    // User did not enable Bluetooth or an error occured
                    Log.d (TAG, "BT not enabled");
                    Toast.makeText (this, R.string.bt_not_enabled, Toast.LENGTH_SHORT).show ();
                    finish ();
                }
        }

        if (D)
        {
            Log.e (TAG, "Leaving:  onActivityResult ()");
        }
    }

    private void enableButtons ()
    {
        if (D)
        {
            Log.e (TAG, "Entering: enableButtons ()");
        }

        touchButton.setEnabled (true);
        sensoryButton.setEnabled (true);

        if (D)
        {
            Log.e (TAG, "Leaving:  enableButtons ()");
        }
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        MenuInflater inflater = getMenuInflater ();
        inflater.inflate (R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item)
    {
        switch (item.getItemId ())
        {
            case R.id.scan:
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent (this, DeviceListActivity.class);
                startActivityForResult (serverIntent, REQUEST_CONNECT_DEVICE);
                return true;
            case R.id.discoverable:
                // Ensure this device is discoverable by others for 300 seconds
                if (bluetoothAdapter.getScanMode () != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
                {
                    Intent discoverableIntent = new Intent (BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    discoverableIntent.putExtra (BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                    startActivity (discoverableIntent);
                }
                else
                {
                    Toast.makeText (this, "Not able to make device discoverable", Toast.LENGTH_LONG).show ();
                }
                return true;
        }

        return false;
    }

    public static AndroATuinBluetoothService getBluetoothService ()
    {
        return bluetoothService;
    }
}