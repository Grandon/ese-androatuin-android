package at.technikum.bicss.ese.androatuin;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

public class AndroATuinSensoryActivity
    extends Activity
    implements SensorEventListener
{
    // Debugging
    private static final String        TAG                 = "[AndroATuin][Sensory]";
    private static final boolean       D                   = false;

    private static final int           MIN_Y               = 8;
    private static final int           MIN_Z               = 8;

    private static final int           MAX_Y               = 30;
    private static final int           MAX_Z               = 30;

    // Multiplier is used to calculcate the engine command value
    private static final float         MULTIPLIER_Y        = 100f / MAX_Y;
    private static final float         MULTIPLIER_Y_ONLY   = 100f  / MAX_Y;
    private static final float         MULTIPLIER_Z        = 200f / MAX_Z;

    // Starting Position needed for calibration
    private int                        starting_position_y = 0;
    private int                        starting_position_z = 0;
    
    // Let's only send every n-command to the robo
    private static final int           TIMES               = 3;
    private static final boolean       SLOW_DOWN           = false;
    private int                        counter             = 0;
    
    private SensorManager              sensorManager       = null;

    private TextView                   xAxis               = null;
    private TextView                   yAxis               = null;
    private TextView                   zAxis               = null;

    private TextView                   engineCommandView   = null;

    private ToggleButton               onOffButton         = null;
    private Button                     calibrateButton     = null;
    private Button                     resetButton         = null;
    
    // Bluetooth Service
    private AndroATuinBluetoothService bluetoothService    = null;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);

        if (D)
        {
            Log.d (TAG, "Entering: onCreate (Bundle savedInstanceState)");
        }
        
        setContentView (R.layout.sensory);

        xAxis = (TextView)findViewById (R.id.TextView04);
        yAxis = (TextView)findViewById (R.id.TextView05);
        zAxis = (TextView)findViewById (R.id.TextView06);

        bluetoothService = AndroATuinActivity.getBluetoothService ();
        
        engineCommandView = (TextView)findViewById (R.id.TextView08);
        
        calibrateButton = (Button)findViewById (R.id.Button01);
        
        calibrateButton.setOnClickListener (new View.OnClickListener ()
        {
            public void onClick (View v)
            {
                int starting_position_y_new = 0;
                int starting_position_z_new = 0;
                
                starting_position_y_new = Integer.parseInt (yAxis.getText ().toString ());
                starting_position_z_new = Integer.parseInt (zAxis.getText ().toString ());
                
                if (D)
                {
                    Log.d (TAG, "Calibrate Button Pressed! New Starting Position Y = " + starting_position_y_new + " / Z = " + starting_position_z_new);
                }
                
                starting_position_y = starting_position_y_new;
                starting_position_z = starting_position_z_new;
            }
        });
        
        resetButton = (Button)findViewById (R.id.Button02);
        
        resetButton.setOnClickListener (new View.OnClickListener ()
        {
            public void onClick (View v)
            {
                starting_position_y = 0;
                starting_position_z = 0;
                
                if (D)
                {
                    Log.d (TAG, "Reset Button Pressed! New Starting Position Y = " + starting_position_y + " / Z = " + starting_position_z);
                }
            }
        });
        
        onOffButton = (ToggleButton)findViewById (R.id.toggleButton1);
        
        onOffButton.setOnClickListener (new View.OnClickListener ()
        {
            public void onClick (View v)
            {   
                if (!onOffButton.isChecked ())
                {
                    if (bluetoothService != null)
                    {
                        bluetoothService.move (Directions.STOP);
                    }
                }
            }
        });
        
        sensorManager = (SensorManager)getSystemService (Context.SENSOR_SERVICE);
        
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (D)
        {
            Log.d (TAG, "Leaving:  onCreate (Bundle savedInstanceState)");
        }
    }

    @SuppressWarnings ("deprecation")
    @Override
    protected void onResume ()
    {
        super.onResume ();
        sensorManager.registerListener (this, sensorManager.getDefaultSensor (Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_UI);
    }

    @SuppressWarnings ("deprecation")
    @Override
    protected void onStop ()
    {
        super.onStop ();
        sensorManager.unregisterListener (this, sensorManager.getDefaultSensor (Sensor.TYPE_ORIENTATION));
    }

    @Override
    public void onAccuracyChanged (final Sensor sensor, final int accuracy)
    {
        // Don't need that
    }

    @Override
    public void onSensorChanged (final SensorEvent event)
    {
        EngineCommandRequest request = null;
        
        final int xAxisValue = (int) event.values[0];
        final int yAxisValue = (int) event.values[1];
        final int zAxisValue = (int) event.values[2];
        
        xAxis.setText (String.valueOf (xAxisValue));
        yAxis.setText (String.valueOf (yAxisValue));
        zAxis.setText (String.valueOf (zAxisValue));
        
        final int yAxisValueCalibrated = yAxisValue - starting_position_y;
        final int zAxisValueCalibrated = zAxisValue - starting_position_z;
        
        if (D)
        {
            Log.d (TAG, "Calibrated Y Axis Value = " + yAxisValueCalibrated);
            Log.d (TAG, "Calibrated Z Axis Value = " + zAxisValueCalibrated);
        }
        
        int yAxisValuePositive = Math.abs (yAxisValueCalibrated);
        
        if (yAxisValuePositive > MAX_Y)
        {
            yAxisValuePositive = MAX_Y;
        }
        
        int zAxisValuePositive = Math.abs (zAxisValueCalibrated);
        
        if (zAxisValuePositive > MAX_Z)
        {
            zAxisValuePositive = MAX_Z;
        }
        
        request = new EngineCommandRequest ();
        
        boolean yAxisEnabled  = (yAxisValueCalibrated > MIN_Y || yAxisValueCalibrated < -MIN_Y);
        boolean yAxisPositive = (yAxisValueCalibrated > MIN_Y);
        
        boolean zAxisEnabled  = (zAxisValueCalibrated > MIN_Z || zAxisValueCalibrated < -MIN_Z);
        boolean zAxisPositive = (zAxisValueCalibrated > MIN_Z);
        
        if (D)
        {
            Log.d (TAG, "yAxisEnbled? " + yAxisEnabled + " yAxisPositive?" + yAxisPositive);
            Log.d (TAG, "zAxisEnbled? " + zAxisEnabled + " zAxisPositive?" + zAxisPositive);
        }
        
        // Check if we should move at all
        if (yAxisEnabled || zAxisEnabled)
        {
            if (zAxisEnabled && !yAxisEnabled) // Only UP or DOWN
            {
                if (!zAxisPositive)
                {
                    request.setLeftForward  (true);
                    request.setRightForward (true);
                }
                else
                {
                    request.setLeftForward  (false);
                    request.setRightForward (false);
                }
                
                request.setLeft  ((int)(zAxisValuePositive * MULTIPLIER_Z));
                request.setRight ((int)(zAxisValuePositive * MULTIPLIER_Z));
            }
            else if (yAxisEnabled && !zAxisEnabled) // Only LEFT or RIGHT
            {   
                if (yAxisPositive) // LEFT
                {
                    request.setLeftForward  (false);
                    request.setRightForward (true);
                    request.setLeft  (0);
                    request.setRight ((int)(yAxisValuePositive * MULTIPLIER_Y_ONLY));
                }
                else // RIGHT
                {
                    request.setLeftForward  (true);
                    request.setRightForward (false);
                    request.setLeft  ((int)(yAxisValuePositive * MULTIPLIER_Y_ONLY));
                    request.setRight (0);
                }
            }
            else
            {
                if (!zAxisPositive) // FORWARD
                {
                    request.setLeftForward  (true);
                    request.setRightForward (true);
                }
                else // BACKWARD
                {
                    request.setLeftForward  (false);
                    request.setRightForward (false);
                }
             
                if (yAxisPositive) // LEFT
                {
                    request.setLeft  ((int)(100f - yAxisValuePositive * MULTIPLIER_Y) * 2);
                    request.setRight ((int)(zAxisValuePositive * MULTIPLIER_Z));
                }
                else // RIGHT
                {
                    request.setLeft  ((int)(zAxisValuePositive * MULTIPLIER_Z));
                    request.setRight ((int)(100f - yAxisValuePositive * MULTIPLIER_Y) * 2);
                }
            }
        }
        else
        {
            // Sending Stop Command
            request.setLeftForward  (true);
            request.setRightForward (true);
            request.setLeft         (0);
            request.setRight        (0);
        }
        
        if (onOffButton.isChecked ())
        {   
            engineCommandView.setText (request.getPlainEngineCommand ());
            
            ++counter;
            
            if (!SLOW_DOWN && counter % TIMES == 0)
            {
                counter = 0;
                
                if (bluetoothService != null)
                {
                    bluetoothService.move (request);
                }
            }
        }
        else
        {
            engineCommandView.setText ("EL0R0");
            
            if (D)
            {
                Log.d (TAG, "Sending of Engine Command is disabled");
            }
        }
    }

}
