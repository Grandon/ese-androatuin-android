package at.technikum.bicss.ese.androatuin;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;

public class AndroATuinTouchActivity
    extends Activity
    implements OnTouchListener
{
    // Debugging
    private static final String        TAG              = "[AndroATuin][Touch]";
    private static final boolean       D                = false;

    // Bluetooth Service
    private AndroATuinBluetoothService bluetoothService = null;

    // Arrows
    private ImageView                  imageView1       = null;
    private ImageView                  imageView2       = null;
    private ImageView                  imageView3       = null;
    private ImageView                  imageView4       = null;
    private ImageView                  imageView5       = null;
    private ImageView                  imageView6       = null;
    private ImageView                  imageView7       = null;
    private ImageView                  imageView8       = null;
    private ImageView                  imageView9       = null;

    // Last Pressed Image View
    private int                        lastImageViewId  = 0;
    
    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.touch);
        
        if (D)
        {
            Log.e (TAG, "Entering: onCreate (Bundle savedInstanceState)");
        }
        
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        bluetoothService = AndroATuinActivity.getBluetoothService ();
        
        imageView1 = (ImageView)findViewById (R.id.imageView1);
        imageView1.setOnTouchListener (this);
        
        imageView2 = (ImageView)findViewById (R.id.imageView2);
        imageView2.setOnTouchListener (this);

        imageView3 = (ImageView)findViewById (R.id.imageView3);
        imageView3.setOnTouchListener (this);

        imageView4 = (ImageView)findViewById (R.id.imageView4);
        imageView4.setOnTouchListener (this);

        imageView5 = (ImageView)findViewById (R.id.imageView5);
        imageView5.setOnTouchListener (this);

        imageView6 = (ImageView)findViewById (R.id.imageView6);
        imageView6.setOnTouchListener (this);

        imageView7 = (ImageView)findViewById (R.id.imageView7);
        imageView7.setOnTouchListener (this);

        imageView8 = (ImageView)findViewById (R.id.imageView8);
        imageView8.setOnTouchListener (this);

        imageView9 = (ImageView)findViewById (R.id.imageView9);
        imageView9.setOnTouchListener (this);
        
        if (D)
        {
            Log.e (TAG, "Leaving: onCreate (Bundle savedInstanceState)");
        }
    }

    @Override
    public boolean onTouch (View view, MotionEvent event)
    {
        Boolean pressed = null;

        if (event.getAction () == MotionEvent.ACTION_DOWN)
        {
            pressed = true;
        }
        else if (event.getAction () == MotionEvent.ACTION_UP)
        {
            pressed = false;
        }

        // We are only interested in DOWN/UP (PRESSED/NOT_PRESSED_ANYMORE) Actions
        if (pressed != null && pressed == true)
        {
            if (D)
            {
                Log.d (TAG, "On Touch Event occured from View with ID = " + view.getId ());
            }

            lastImageViewId = view.getId ();
            
            switch (view.getId ())
            {
                case R.id.imageView1:
                    if (D)
                    {
                        Log.d (TAG, ">>>UP LEFT<<<");
                        imageView1.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up_left_pressed));
                        bluetoothService.move (Directions.UP_LEFT);
                    }
                break;
                case R.id.imageView2:
                    if (D)
                    {   
                        Log.d (TAG, ">>>  UP   <<<");
                        imageView2.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up_pressed));
                        bluetoothService.move (Directions.UP);
                    }
                break;
                case R.id.imageView3:
                    if (D)
                    {
                        Log.d (TAG, ">>>UP RIGHT<<<");
                        imageView3.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up_right_pressed));
                        bluetoothService.move (Directions.UP_RIGHT);
                    }
                break;
                case R.id.imageView4:
                    if (D)
                    {
                        Log.d (TAG, ">>>  LEFT  <<<");
                        imageView4.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_left_pressed));
                        bluetoothService.move (Directions.LEFT);
                    }
                break;

                case R.id.imageView5:
                    if (D)
                    {
                        Log.d (TAG, ">>> CENTER <<<");
                        imageView5.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.center_pressed));
                        bluetoothService.move (Directions.STOP);
                    }
                break;

                case R.id.imageView6:
                    if (D)
                    {
                        Log.d (TAG, ">>>  RIGHT  <<<");
                        imageView6.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_right_pressed));
                        bluetoothService.move (Directions.RIGHT);
                    }
                break;

                case R.id.imageView7:
                    if (D)
                    {
                        Log.d (TAG, ">>>DOWN LEFT<<<");
                        imageView7.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_down_left_pressed));
                        bluetoothService.move (Directions.DOWN_LEFT);
                    }
                break;

                case R.id.imageView8:
                    if (D)
                    {
                        Log.d (TAG, ">>>   DOWN  <<<");
                        imageView8.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_down_pressed));
                        bluetoothService.move (Directions.DOWN);
                    }
                break;

                case R.id.imageView9:
                    if (D)
                    {
                        Log.d (TAG, ">>>DOWN RIGHT<<<");
                        imageView9.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_down_right_pressed));
                        bluetoothService.move (Directions.DOWN_RIGHT);
                    }
                break;

                default:
                    if (D)
                    {
                        Log.e (TAG, "Where the hell did you come from?");
                    }
            }
        }
        else if (pressed != null && pressed == false)
        {
            if (D)
            {
                Log.d (TAG, "View ID = " + view.getId ());
            }
            
            resetImage();
            Log.d (TAG, ">>>STOP<<<");
            bluetoothService.move (Directions.STOP);
        }

        return true;
    }

    private void resetImage ()
    {
        if (D)
        {
            Log.d (TAG, "Resetting Image with View ID = " + lastImageViewId);
        }
        
        if (lastImageViewId != 0)
        {
            switch (lastImageViewId)
            {
                case R.id.imageView1:
                    if (D)
                    {
                        imageView1.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up_left));
                    }
                break;
                case R.id.imageView2:
                    if (D)
                    {   
                        imageView2.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up));
                    }
                break;
                case R.id.imageView3:
                    if (D)
                    {
                        imageView3.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up_right));
                    }
                break;
                case R.id.imageView4:
                    if (D)
                    {
                        imageView4.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_left));
                    }
                break;

                case R.id.imageView5:
                    if (D)
                    {
                        imageView5.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.center));
                    }
                break;

                case R.id.imageView6:
                    if (D)
                    {
                        imageView6.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_right));
                    }
                break;

                case R.id.imageView7:
                    if (D)
                    {
                        imageView7.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_down_left));
                    }
                break;

                case R.id.imageView8:
                    if (D)
                    {
                        imageView8.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_down));
                    }
                break;

                case R.id.imageView9:
                    if (D)
                    {
                        imageView9.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.arrow_down_right));
                    }
                break;

                default:
                    if (D)
                    {
                        Log.e (TAG, "Where the hell did you come from?");
                    }
            }
        }
    }
}
