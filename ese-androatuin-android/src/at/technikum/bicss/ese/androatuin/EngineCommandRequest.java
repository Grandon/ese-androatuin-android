package at.technikum.bicss.ese.androatuin;

public class EngineCommandRequest
{
    private static final char COMMAND      = 'E';

    private static final int  MIN_POWER    = 0;
    private static final int  MAX_POWER    = 200;

    private boolean           leftForward  = true;
    private int               left         = 0;
    private boolean           rightForward = true;
    private int               right        = 0;

    public EngineCommandRequest ()
    {
        super ();
    }
    
    public EngineCommandRequest (final boolean leftForward, final int left, final boolean rightForward, final int right)
    {
        super ();

        this.leftForward    = leftForward;
        this.left           = left;
        this.rightForward   = rightForward;
        this.right          = right;
    }
    
    public byte[] toByteArray ()
    {
        byte[] retValue = null;
        
        retValue = new byte[5];

        retValue[0] = COMMAND;
        
        if (leftForward)
        {
            retValue[1] = 1;
        }
        else
        {
            retValue[1] = 0;
        }
        
        retValue[2] = (byte) left;
        
        if (rightForward)
        {
            retValue[3] = 1;
        }
        else
        {
            retValue[3] = 0;
        }
        
        retValue[4] = (byte) right;
        
        return retValue;
    }

    @Override
    public String toString ()
    {
        byte[] bytePresentation = toByteArray ();
        
        return  "Engine Command: " +
                COMMAND +
                (leftForward ? "L" : "l") +
                left +
                (rightForward ? "R" : "r") +
                right +
                " / Bytes: " + 
                toUnsignedByte (bytePresentation[0]) + "," +
                toUnsignedByte (bytePresentation[1]) + "," +
                toUnsignedByte (bytePresentation[2]) + "," +
                toUnsignedByte (bytePresentation[3]) + "," +
                toUnsignedByte (bytePresentation[4]);
    }
    
    public String getPlainEngineCommand ()
    {
        return COMMAND +
            (leftForward ? "L" : "l") +
            left +
            (rightForward ? "R" : "r") +
            right;
    }
    
    private short toUnsignedByte (byte b)
    {
        return (short) (b & 0xFF);
    }
    
    public boolean isLeftForward ()
    {
        return leftForward;
    }

    public void setLeftForward (final boolean leftForward)
    {
        this.leftForward = leftForward;
    }

    public int getLeft ()
    {
        return left;
    }

    public void setLeft (final int left)
    {
        if (left > MAX_POWER || left < MIN_POWER)
        {
            this.left = 0;
        }
        else
        {
            this.left = left;
        }
    }

    public boolean isRightForward ()
    {
        return rightForward;
    }

    public void setRightForward (final boolean rightForward)
    {
        this.rightForward = rightForward;
    }

    public int getRight ()
    {
        return right;
    }

    public void setRight (final int right)
    {
        if (right > MAX_POWER || right < MIN_POWER)
        {
            this.right = 0;
        }
        else
        {
            this.right = right;
        }
    }

}
