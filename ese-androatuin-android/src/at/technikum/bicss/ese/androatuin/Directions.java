package at.technikum.bicss.ese.androatuin;

public enum Directions
{
    UP          (new EngineCommandRequest (true,  200, true,  200)),
    DOWN        (new EngineCommandRequest (false, 200, false, 200)),
    LEFT        (new EngineCommandRequest (false,  80, true,   80)),
    RIGHT       (new EngineCommandRequest (true,   80, false,  80)),
    UP_LEFT     (new EngineCommandRequest (true,  100, true,  200)),
    UP_RIGHT    (new EngineCommandRequest (true,  200, true,  100)),
    DOWN_LEFT   (new EngineCommandRequest (false, 100, false, 200)),
    DOWN_RIGHT  (new EngineCommandRequest (false, 200, false, 100)),
    STOP        (new EngineCommandRequest (true,    0, true,    0));
    
    private EngineCommandRequest engineCommand = null;

    private Directions (final EngineCommandRequest engineCommand)
    {
        this.engineCommand = engineCommand;
    }
    
    public EngineCommandRequest getEngineCommand ()
    {
        return engineCommand;
    }

}
